# Change Log

## [0.0.8] - 30-06-2020

- Fix substrate string functions to add highlighting
- Change `keyword.operator.elps` scope to `keyword.operator.logical.elps` to support more themes

## [0.0.7] - 17-06-2020

- Add ELPS icon

## [0.0.6] - 11-06-2020

- Support function/macro parameter highlighting!
- Fix broken highlighting for functions without params and any keywords with trailing paranthesis instead of space

## [0.0.5] - 26-05-2020

- Support all substrate builtin functions
- Extend list of ELPS builtin functions

## [0.0.4] - 26-05-2020

- Correctly differentiate between a symbol and shorthand list definition

## [0.0.3] - 26-05-2020

- Include a few more builtin storage functions
- Fix symbol highlighting to allow closure of paranthesis without whitespace

## [0.0.2] - 26-05-2020

- Include most of ELPS's builtin functions


## [0.0.1] - 25-05-2020

- Initial release, basic syntax highlighting
- List of builtin functions not yet included
- Accurate `wordPattern` to aid automatic text suggestion by VSCode
- Grammar written in YAML file, with auto-generated JSON from it
