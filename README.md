# VSCode extension for ELPS

ELPS: https://github.com/luthersystems/elps

I wasn't able to find any existing LISP syntax highlighters that worked well
enough for ELPS in VSCode. This is an extension to fill that gap and provides
many ELPS specific features.

## Features

- An accurate `wordPattern` has been set which allows VSCode to make more
  correct automatic word based suggestions. E.g. function names with `-` in
  their name will be suggested
- Easy to read textMate grammar written in YAML file, and auto-generated to JSON
- Many more scoped supported than other existing LISP syntax highlighters.
  This allows your theme to be able to highlight more components of the code.


## Known Issues

* Space before function not supported - this is intentional for keeping the grammar regexes simple and discouraging placing spaces before function calls

## Development

Useful links:

- https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide
- https://macromates.com/manual/en/language_grammars
- Good article on textMate grammar: https://www.apeth.com/nonblog/stories/textmatebundle.html
- Concise article on Ruby regular expressions: https://www.rubyguides.com/2015/06/ruby-regex/
- Ruby regular expression online editor: https://rubular.com/
- Another project defining text mate grammar in YAML (useful for looking up how certain new keys should be nested as there's no documentation): https://github.com/Togusa09/vscode-tmlanguage/blob/master/syntaxes/yaml-tmlanguage.YAML-tmLanguage
